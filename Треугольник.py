class Triangle:
    
    def __init__(self, a, b, c):
        self.a= a
        self.b= b
        self.c= c
    def Perimetr(self):
        return self.a+self.c+self.b
    def PoluPerimetr(self):
        return (self.a+self.c+self.b)/2
    def Ploshyad(self):
        P=self.PoluPerimetr()
        return (P*(P-self.a)*(P-self.b)*(P-self.c))**(1/2)
MyTriangle = Triangle(6,5,7)
print(MyTriangle.Ploshyad())
print(MyTriangle.Perimetr())
print(MyTriangle.PoluPerimetr())
